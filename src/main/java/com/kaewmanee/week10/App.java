package com.kaewmanee.week10;

public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5,3);
        System.out.println(rec);
        System.out.printf("area : %.3f \n",rec.calArea());
        System.out.printf("perimeter : %.3f \n",rec.calPerimeter());

        System.out.println();

        Rectangle rec2 = new Rectangle(2,2);
        System.out.println(rec2);
        System.out.printf("area : %.3f \n",rec2.calArea());
        System.out.printf("perimeter : %.3f \n",rec2.calPerimeter());

        System.out.println();

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area : %.3f \n",circle1.getName(),circle1.calArea());
        System.out.printf("%s perimeter:  %.3f \n",circle1.getName(),circle1.calPerimeter());

        System.out.println();

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area : %.3f \n",circle2.getName(),circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle2.getName(),circle2.calPerimeter());

        System.out.println();

        Triangle tri1 = new Triangle(5,5,5);
        System.out.println(tri1);
        System.out.printf("%s area : %.3f \n",tri1.getName(),tri1.calArea());
        System.out.printf("%s perimeter:  %.3f \n",tri1.getName(),tri1.calPerimeter());

        System.out.println();

        Triangle tri2 = new Triangle(6.5,5,5);
        System.out.println(tri2);
        System.out.printf("%s area : %.3f \n",tri2.getName(),tri2.calArea());
        System.out.printf("%s perimeter:  %.3f \n",tri2.getName(),tri2.calPerimeter());
    }
}
